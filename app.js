'use strict';

var express = require('express')
var app = express()
var Redis = require('ioredis');

app.get('/', function (req, res) {
  var sentinels = [
    { host: '45.55.200.147', port: 26379 },
    { host: '45.55.255.203', port: 26379 }];
  var redis = new Redis({
    sentinels: sentinels,
    name: 'mymaster',
    password: "1"
  });

  redis.incr('foo', function(err, reply) {
    var message = `Visited: ${reply.toString()} times.`;
    res.send(message);
  });
})

app.listen('9999');